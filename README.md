A default .gitignore file for Django projects
=============================================

This is the default .gitignore for Django projects at www.agbo.biz

To use it, put the gitignoreDjango script somewhere in your PATH and run it.
It will automagically download the latest version and save it in the current directory.

Feel free to add whatever files you believe we forgot to include in the .gitignore file. 